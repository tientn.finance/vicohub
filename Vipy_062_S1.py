import numpy as np

url = "https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data"
a = np.genfromtxt(url, delimiter=",", dtype=np.float, usecols=[0])
print((a-a.min())/(a.max()-a.min()))

