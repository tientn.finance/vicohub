import numpy as np

url="https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data"
a=np.genfromtxt(url,delimiter=",",dtype=np.float)
mask = (a[:, 0] < 5) & (a[:, 2] > 1.5)
b=a[mask]
print(b)